package com.pawelbanasik;

public class Casting {

	public static void main(String[] args) {

		double a = 2.5;
		System.out.println(a);

		int b = (int) a;
		System.out.println(b);

		double c = 5;
		System.out.println(c);

		int d = (int) c / 2;
		System.out.println(d);
		c = d;
		System.out.println(d);

		short sh = (short) d;
		d = sh;

	}

}
